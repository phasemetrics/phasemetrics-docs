\documentclass[11pt,titlepage]{article}
\usepackage{abstract}
\usepackage{changepage}
\usepackage[a4paper,headheight=15pt]{geometry}
\usepackage[myheadings]{fullpage}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{url}
\usepackage[hidelinks,breaklinks]{hyperref}
\usepackage{breakurl}
\usepackage{xcolor}
\usepackage[numbers,sort]{natbib}
\usepackage[urw-garamond]{mathdesign}
\usepackage[T1]{fontenc}
\usepackage[font=small, labelfont=bf]{caption}
\usepackage[english]{babel}
\usepackage{sectsty}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage{tocloft}
\usepackage{graphicx}
\usepackage{float}
\usepackage{pgfgantt}
\usepackage{textcomp}

%-----------------------------------------------------------------
% CONFIG AND COMMANDS
%-----------------------------------------------------------------
\graphicspath{ {images/} }

\renewcommand*{\abstractnamefont}{
	\fontfamily{fos}
	\fontseries{b}
	\fontshape{sc}
	\selectfont
	\Large
}

\renewcommand{\cfttoctitlefont}{
	\fontfamily{fos}
	\fontseries{b}
	\fontshape{sc}
	\selectfont
	\Large
}

\renewcommand{\cftloftitlefont}{
	\fontfamily{fos}
	\fontseries{b}
	\fontshape{sc}
	\selectfont
	\Large
}

% Configure hyperlinks coloring.
\hypersetup{
	colorlinks,
	linkcolor={green!40!black},
	citecolor={blue!50!black},
	urlcolor={blue!80!black}
}

% Add new doi command that inserts url.
\newcommand*{\doi}[1]{\\\href{http://dx.doi.org/#1}{doi: #1}}

% Wide page for side by side figures, tables, etc.
\newlength{\offsetpage}
\setlength{\offsetpage}{1.0cm}
\newenvironment{widepage}{
	\begin{adjustwidth}{-\offsetpage}{-\offsetpage}%
    \addtolength{\textwidth}{2\offsetpage}}%
	{\end{adjustwidth}
}

\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}

\renewcommand{\tocbibname}{References}

%-----------------------------------------------------------------
% HEADER & FOOTER
%-----------------------------------------------------------------
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\small Agent Systems and Applications (Summer 2016)}
\fancyhead[R]{\small Project Concept}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}

\begin{document}

%-----------------------------------------------------------------
% TITLE
%-----------------------------------------------------------------
\title{ \normalsize \textsc{Agent Systems and Applications\\Project Concept}
	\\ [2.0cm]
	\HRule{0.5pt} \\
	\fontfamily{fos}\fontseries{b}\fontshape{n}\selectfont
	\LARGE PhaseMetrics\texttrademark: An Android and PaaS Based Platform for Developing and Deploying Semantically Interoperable Multi-Agent Systems
	\HRule{2pt} \\ [0.5cm]
	\normalsize \vspace{5\baselineskip}
	\normalfont \flushleft
	\textsc{Student:\\Sando George\\
		Faculty of Mathematics and Information Science\\
		Warsaw University of Technology}\\
	\vspace{3\baselineskip}	
	\textsc{Guided By:\\prof. dr hab. Maria Ganzha (IBS PAN)\\
		prof. dr hab. Marcin Paprzycki (IBS PAN)}}


\author{}

\date{Summer 2016}
\maketitle

\begin{abstract}
The applications of sensing enabled multi-agent decision making are ever increasing. Systems in this domain allow for solving problems, through the use of multidimensional data, that could not be solved using traditional monolithic systems. This document explores a project concept that aims to provide application developers within this domain with an extensible framework that will enable rapid prototyping.

\smallskip
\noindent \textbf{Keywords.} Multi-agent systems, Internet of Things, Android
\end{abstract}


\newpage
	\tableofcontents
	\listoffigures

%-----------------------------------------------------------------
% BODY
%-----------------------------------------------------------------
\sectionfont{\fontfamily{fos}\fontseries{b}\fontshape{sc}\selectfont}
\newpage
\section{Introduction}
\emph{\bfseries Multi-agent systems (M.A.S)} are distributed computing environments that are characterized by multiple interacting, and \emph{intelligent}, computational entities \cite{MPSingh94}. M.A.S have great significance in the ever advancing applications of computer science. This significance was born out of the realization that collaborating computing systems provide a platform for solving problems that could not be solved by traditional monolithic systems. Using multidimensional data emanating from disparate systems, better models can be developed to represent the state of an environment, and, as such, better decisions can be made on effecting changes in that environment.

According to \citet*{CMElliot2014}, ``beyond academic opportunities, tomorrow's (if not already today's) technology will drive a demanding need for pressing forward in the realm of multi-agent control''. When M.A.S is synthesized with the emerging field of \emph{\bfseries Internet of Things (IoT)}, new application domains are exposed in markets ranging from transportation and healthcare to industrial automation and disaster response. Current projections indicate that, by the year 2025, manufacturing and healthcare will account for as much as 74\% of the IoT market \cite{AlFuqaha2015}. There will not be a better time, than now, to join research and development initiatives in this area.

All this paints a good picture for the synthesis of the fields of M.A.S and IoT. There is, however, at least one challenge that will stem from this synthesis. This is the challenge of \emph{\bfseries semantic interoperability}. Disparate systems collaborating towards a common goal need to \emph{``talk''} to each other and understand what is being said. Any application in this domain, therefore, will have to make semantic interoperability a prime consideration.

\section{Problem Definition}
Given the rapid advancement of technology in the M.A.S and IoT domains, and the prospects of bridging the two domains, there should be some amount of research targeted at developing reference models for building applications at their point of convergence.

Taking it a step further, there should be a platform that allows developers to quickly prototype applications in the convergent domain without necessarily having technical expertise in all of the parent domains. Such a platform does not exist. Because of this shortcoming, anyone considering developing applications in the convergent domain will have to invest more resources into product development activities than if such a platform existed. This will in turn increase the time-to-market of applications in the domain and possibly limit competition to entities with significant financial backing.

\section{Solution}
Targeting the convergent domain, in a mobile environment, we are proposing the design and development of a platform that allows rapid prototyping of applications. This platform will leverage the pervasiveness of the Android operating system and the sensing capabilities of current smartphones. According to the \citet{IDC2015}, in the second quarter of the year 2015, Android accounted for 82.8\% of smartphones on the global market.

\subsection{Platform Architecture}
The platform will consist of two main applications. An Android application, based on a plugin architecture, and a Platform as a Service (PaaS) application.

\paragraph{The Android application} will be extendable via plugins. It will provide access to the sensing capabilities of the device, agent communication and authentication with the PaaS application.

\paragraph{The PaaS application} will provide agent management facilities, data storage and analysis capabilities, and account management for plugin developers.

\begin{figure}[H]
	\begin{widepage}
		\centering
		\includegraphics[width=0.9\textwidth]{arch.eps}
	\end{widepage}
	\caption{Suggested platform architecture.}
	\label{fig:PlatformArch}
\end{figure}

\subsection{Design Considerations}
In order to make the platform a success, several factors must be taken into consideration during the analysis and design phase. Some of the factors here have been adapted from those mentioned by \citet{MMurphy2013}.

\subsubsection{Semantic Interoperability}
If the proposed system is to advance, in terms of the complexity of problems it can be applied to, consideration has to be given to embedding semantic capabilities into the system. There should be set schemes on how data is stored and communicated between agents in the system. This will allow plugins to leverage each other's capabilities without implementing multiple data processing schemes.

\subsubsection{Plugin Discovery}
The issue of plugin discovery can be looked at from two perspectives. These are plugin discovery by users and plugin discovery and registration by the application.

\paragraph{Users} need a way to discover which plugins are available to extend the functionality of the application. Since the plugins will be distributed independently via the Google Play Store marketplace, users can be pointed to these plugins via a prompt in the application or via a list on the PaaS website.

\paragraph{The parent application} needs a way to detect available plugins and when plugins are installed or removed on a user device, preferably in real time. This would make it a requirement that the parent application be installed first.

\subsubsection{Inter Process Communication}
In order to solve the specific problems that they will be applied to, plugins need a way of communicating with the parent application and with other plugins. This should be a simple interface that will allow developers to avoid highly technical implementation details.

\subsubsection{Security}
In order to ensure the security and, inherently, the privacy of users, the parent application should only allow communication with plugins that have been registered with the PaaS application and authorized by the user. In addition to this, it should not be possible for third party applications to \emph{eavesdrop} on this communication.

\subsection{Business Model}
The application should adopt a \emph{``freemium''} strategy where use of the basic system is free of charge but charges are attached to more advanced functionality. This will ease friction in the application adoption process. Possible revenue streams include:

\begin{itemize}
	\item charging developers to host multiple plugins.
	\item charging developers for increased computing and storage resources.
	\item plugin development consultation fees.
\end{itemize}

Developers should be able to design their own revenue generation schemes.

\section{Project Validation}
The project is to be validated through the implementation of a working demonstration plugin.

\newpage
\section{Proposed Schedule}
\paragraph{Tasks} \textbf{T1.} Requirements Gathering \textbf{T2.} System Specification \textbf{T3.} Technical Specification \textbf{T4.} PaaS App Development \textbf{T5.} Android App Development \textbf{T6.} Demo Plugin Development \textbf{T7.} Reporting \textbf{T8.} Testing and Control.

\begin{figure}[H]
	\begin{widepage}
	\centering
	\begin{ganttchart}[y unit title=0.4cm,
						y unit chart=0.5cm,
						vgrid,hgrid, 
						title label anchor/.style={below=-1.6ex},
						title left shift=.05,
						title right shift=-.05,
						title height=1,
						bar/.style={fill=gray!50},
						incomplete/.style={fill=white},
						progress label text={},
						bar height=0.5,
						group right shift=0,
						group top shift=.6,
						group height=.3
		]{1}{30}
		\gantttitle{Weeks}{30} \\
		\gantttitle{1}{2}
		\gantttitle{2}{2}
		\gantttitle{3}{2}
		\gantttitle{4}{2}
		\gantttitle{5}{2}
		\gantttitle{6}{2}
		\gantttitle{7}{2}
		\gantttitle{8}{2}
		\gantttitle{9}{2}
		\gantttitle{10}{2}
		\gantttitle{11}{2}
		\gantttitle{12}{2}
		\gantttitle{13}{2}
		\gantttitle{14}{2}
		\gantttitle{15}{2} \\
		
		\ganttbar{T1}{3}{4} \\
		\ganttbar{T2}{5}{6} \\
		\ganttbar{T3}{7}{8} \\
		\ganttbar{T4}{9}{16} \\
		\ganttbar{T5}{13}{24} \\
		\ganttbar{T6}{21}{28} \\
		\ganttbar{T7}{3}{28} \\
		\ganttbar{T8}{3}{28}
		
%		\ganttlink{elem0}{elem1}
	\end{ganttchart}
	\end{widepage}
	\caption{Gantt Chart for Project Work}
\end{figure}

\section{Project Deliverables}
The project deliverables will include:

\begin{enumerate}
	\item Requirements Specification (Week 3)
	\item System Specification (Week 4)
	\item Technical Specification (Week 5)
	\item Final Report (Week 15)
	\item Applications Code (Week 15)
	\item Presentation slides on instructor appointed dates.
\end{enumerate}

\newpage
\bibliography{citations}
\bibliographystyle{plainnat}
\end{document}
